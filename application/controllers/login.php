<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class login  extends CI_Controller {

    function __construct() {
        parent::__construct();
// $this->output->enable_profiler(TRUE);
    }


     function index() {
     if($this->session->userdata('loged_in')){
		redirect('dashboard', 'refresh');
		}else{
			$this->load->view('include/head');
            $this->load->view('login_view');
		}            
            }

    function registration(){
    	
    if($this->session->userdata('loged_in')){
		redirect('dashboard', 'refresh');
		}         if($this->input->post('submit')=='submit'){
        $this->load->library('form_validation');
         $config_rule = array(
               array(
                     'field'   => 'fullname',
                     'label'   => 'fullname',
                     'rules'   => 'required|xss_clean'
                     ),
               array(
                     'field'   => 'password',
                     'label'   => 'Password',
                     'rules'   => 'required|xss_clean|matches[repassword]'
                     ),
               array(
                     'field'   => 'repassword',
                     'label'   => 'Password Confirmation',
                     'rules'   => 'required|xss_clean'
                     ),
               array(
                     'field'   => 'address',
                     'label'   => 'address',
                     'rules'   => 'required|xss_clean'
                     ),
               array(
                     'field'   => 'email',
                     'label'   => 'email',
                     'rules'   => 'required|xss_clean|callback_email_check'
                     ),
               array(
                     'field'   => 'city',
                     'label'   => 'city',
                     'rules'   => 'required|xss_clean'
                     ),

                    array(
                     'field'   => 'gender',
                     'label'   => 'gender',
                     'rules'   => 'required|xss_clean'
                     ),


               array(
                     'field'   => 'username',
                     'label'   => 'username',
                     'rules'   => 'required|xss_clean|min_length[5]|max_length[12]|callback_username_check'
                     ),
               array(
                     'field'   => 'agree',
                     'label'   => 'agree terms and conditions',
                     'rules'   => 'required|xss_clean'
                     )
                        );

   $this->form_validation->set_rules($config_rule);
   if($this->form_validation->run() == FALSE)
         {
           $this->load->view('include/head');
           $this->load->view('registration');

         }
         else{
	          $postdata = $this->input->post();

              $this->load->model('login_model');
	         $result = $this->login_model->registration($postdata);                if($result){                	$data['msg'] = "registered successfully.";                }else{                	$data['msg'] = "failed";                }
                $this->load->view('include/head');
            $this->load->view('registration',$data);
              }

           }else{
            $this->load->view('include/head');
            $this->load->view('registration');
            }

              }




             public function username_check($username)
	{
        $this->load->model('login_model');
      $result = $this->login_model->checkuser($username); /* send the username to the checkuser helper */


     if($result){ /* check data has somw value or not */
         $this->form_validation->set_message('username_check', 'Username already exist.'); /* set message to form_validation */
         return false;
                     }else{
                        return true;
                           }
   }


     public function email_check($email)
	{
        $this->load->model('login_model');
      $result = $this->login_model->checkemail($email); /* send the username to the checkuser helper */


     if($result){ /* check data has somw value or not */
         $this->form_validation->set_message('email_check', 'email already exist.'); /* set message to form_validation */
         return false;
                     }else{
                        return true;
                           }
   }


            }