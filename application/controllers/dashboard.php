<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    
	public function index()
	{
		
		if($this->session->userdata('loged_in')){
	    $user = $this->session->userdata('loged_in');
	    $data['name'] = $user['user_name'];
	    $data['user_id_pk'] = $user['user_id_pk'];
	    
		 $this->load->model('symbols_model');/* load the Symbols model */
        $res = $this->symbols_model->getsymbols(); /* load Symbols form current */
        if ($res) {   /*check if res coming from model is true or not */
            $data ['symbols'] = $res;
        } else {
            $data['error'] = 'No topic found.. Please add the topic first.';
        }
       
    	    
		$this->load->view('include/inner_head');
        $this->load->view('dashboard_view',$data);
		}else{
		 redirect('login', 'refresh');
		}
		//$this->load->view('welcome_message');
	}
	
	
    function logout()
     {
        $this->session->unset_userdata('logged_in');
       $this->session->sess_destroy();
       redirect('login', 'refresh');
     }

    function stock_symbols(){
     $this->load->model('symbols_model');
        $res = $this->symbols_model->getsymbols();
        echo json_encode($res);
    }
    
    function stock_history(){
        $this->load->model('symbols_model');
        $res = $this->symbols_model->getsymbols();
        echo json_encode($res);
    }

    function recent_search(){
        $this->load->model('symbols_model');
        $res = $this->symbols_model->getRecentHistory();
        echo json_encode($res);
    }

    function getSymbols(){
        $this->load->model('symbols_model');
        $result = $this->symbols_model->getPageSymbols($this->input->get('term'));
        echo json_encode($result);
    }
   
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */