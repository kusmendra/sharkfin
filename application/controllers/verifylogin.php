<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class VerifyLogin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->output->enable_profiler(TRUE);
    }
 function index() {

            $this->load->library('form_validation');
            $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

        if ($this->form_validation->run() == FALSE) {


            $this->load->view('include/head');
            $this->load->view('login_view');
        }
        else{
                                        /* Go to private are and redirect to the home page */
          redirect('dashboard', 'refresh');
            }

 } /* end of the method */

      function check_database($password) {
//Field validation succeeded.  Validate against database
        $username = $this->input->post('username');
//query the database
        $this->load->model('login_model');

        $result = $this->login_model->login($username, $password);

        if ($result != '') {
            $row = $result[0];

            $sess_array = array(
                'user_id_pk' => $row->user_id_pk,
                'user_name' => $row->user_name,
                'fullname' => $row->fullname,

            );


            $this->session->set_userdata('loged_in', $sess_array);

            return TRUE;
        } else {
            $this->form_validation->set_message('check_database', 'Invalid Email or password');
            return false;
        }
    }

    }

