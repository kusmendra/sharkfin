<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Getdata extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	
		
	}
	
    function getsymbol($sym = NULL)
	{
	 	if($sym != NULL){
 	
 	    $this->load->model('symbols_model');/* load the Symbols model */
        $res = $this->symbols_model->getsymbolbyid($sym); /* load Symbols form current */
        if ($res) {
        	// echo json_encode($res);	
        	$data = array($res[0]->company_name,$res[0]->category,$res[0]->country,$res[0]->ticker_name);
		    echo json_encode($data);	
        }
    
 	}
 	else
 	{
 	die("Invalid Request.");
 	}
		
	}
	
	
function get($sym = NULL)
 {
 	if($sym != NULL){
 	
 	$this->load->model('symbols_model');/* load the Symbols model */
        $res = $this->symbols_model->getdata($sym); /* load Symbols form current */
        if ($res) {   /*check if res coming from model is true or not */
        	$jsondata = '[';
           foreach ($res as $record) {
           if($record->volume > 0){
			$valume = $record->volume;
		}else {
			$valume = 0;
		}
		$jsondata .= '['.strtotime($record->date).'000,'.$record->open.','.$record->high.','.$record->low.','.$record->close.','.$valume.'],';
		      }
           $jsondata = rtrim($jsondata, ',');
			echo $jsondata .= ']';
        }
    
 	}
 	else
 	{
 	die("Invalid Request.");
 	}
 }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */