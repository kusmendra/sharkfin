 <?php 
 class Symbols_model extends CI_Model {
 /* This function is for listing  the symbols.
     * @param
     * @return on success->object, failure->false
     */
    function getsymbols() {
        $this->db->select('*');
        $this->db->from('stock_symbols');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {            return $query->result();        } else {            return false;        }
    }/*end of manage topic */
    
    
     function getsymbolbyid($sym){
        $this->load->library('session');
        $userdata=$this->session->userdata('loged_in');

        $this->db->insert('users_history',array('symbol_id'=>$sym,'user_id_ck'=>$userdata['user_id_pk'],'add_date'=>date('Y-m_d')));

        $this->db->select('*');
        $this->db->from('stock_symbols');
        $this->db->where('symbol_id', $sym);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }/*end of manage topic */
    
    
    
     function getdata($sym) {
     	
        $this->db->select('*');
        $this->db->from('historical_data');
        $this->db->where('symbol', $sym);
        $this->db->order_by("date", "asc"); 

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }/*end of manage topic */


    function getRecentHistory(){
        $this->load->library('session');
        if($this->session->userdata('loged_in')){
            $sessionData=$this->session->userdata('loged_in');
            $query = $this->db->query("SELECT distinct ss.* FROM users_history uh, stock_symbols ss where ss.symbol_id=uh.symbol_id and uh.user_id_ck=".$sessionData['user_id_pk']." order by id desc limit 10 ;");
            return $query->result_array();
        }
        return null;

    }

    function getPageSymbols($q){
        $query = $this->db->query("SELECT * FROM stock_symbols ss left join historical_data hd on ss.ticker_name=hd.symbol where ss.ticker_name like '".$q."%' group by hd.`symbol` order by ss.ticker_name limit 100");
        return $query->result_array();
    }
 }
    ?>