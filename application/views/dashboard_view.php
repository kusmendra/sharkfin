<body ng-app>
<section id="container" ng-controller="sharkController"> 
  <!--header start-->
  <header class="header fixed-top clearfix"> 
    <!--logo start-->
    <div class="brand"> <a href="index.php" class="logo"><img src="<?php echo $this->config->base_url();?>application/views/images/logo.png" alt="Sharkfin Traders Technology" title="Sharkfin Traders Technology"></a>
      <div class="sidebar-toggle-box">
        <div class="fa fa-bars"></div>
      </div>
    </div>
    <!--logo end-->     
    <!-- network -->
    <div style="float: left;margin-top: 23px;margin-left: 20px;"> </div>
   <!-- end network -->
    <div class="top-nav clearfix"> 
      <!--search & user info start-->
      <ul class="nav pull-right top-menu">
        <!-- user login dropdown start-->
        <li class="dropdown"> <a data-toggle="dropdown" class="dropdown-toggle" href="#"> <i class=" fa fa-user"></i> <span class="username"> <?php echo $name; ?> </span> <b class="caret"></b> </a>
          <ul class="dropdown-menu extended logout">
            <li><a href="#"><i class=" fa fa-user"></i>Profile</a></li>
            <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
            <li><a href="<?php echo $this->config->site_url()?>/dashboard/logout"><i class="fa fa-power-off"></i> Log Out</a></li>
          </ul>
        </li>
        <!--user login dropdown end -->
      </ul>
      <!--search & user info end--> 
    </div>
  </header>
  <!--header end-->
  <aside>
    <div id="sidebar" class="nav-collapse"> 
      <!-- sidebar menu start-->
      <div class="leftside-navigation">
        <ul class="sidebar-menu" id="nav-accordion">
          <li class="home"> <a href="index.php"> <span class="ico ico-home3"></span> <span>Home</span> </a> </li>
        </ul>
      </div>
      <!-- sidebar menu end--> 
    </div>
  </aside>
  <!--sidebar end--> 
  <!--main content start-->
  <section id="main-content">
    <section class="wrapper"> 
      <!-- page start-->
      <div class="row" id="chart-filter">
		<div class="btn-row col-lg-6" id="stock-search">
          <div class="btn-group">
            <div class="btn btn-danger btn-sm">Stocks</div>
            <input type="hidden" id="symSearch" name="symSearch" style="width:200px;" />
          </div>
          <div class="btn-group">
            <div class="btn btn-danger btn-sm">Recent</div>
            <input type="hidden" id="historySearch" name="historySearch" style="width:150px;" />
          </div>
        </div>
        <div class="btn-row col-lg-6 text-right" id="period">
          <div id="sharkFins" class="btn-group" data-toggle="buttons">
            <div class="btn btn-danger btn-sm">Sharkfin</div>
            <label class="btn btn-default btn-sm">
              <input type="radio" name="tickp" class="ftime" value="1">
              1M </label>
            <label class="btn btn-default btn-sm">
              <input type="radio" name="tickp" class="ftime" value="3">
              3M </label>
            <label class="btn btn-default btn-sm">
              <input type="radio" name="tickp" class="ftime" value="6">
              6M </label>
          </div>
          <div id="sharkPeriods" class="btn-group" data-toggle="buttons">
            <div class="btn btn-danger btn-sm">Period</div>
            <label class="btn btn-default btn-sm">
              <input type="radio" name="timep" class="ptime" value="0">
              1M </label>
            <label class="btn btn-default btn-sm">
              <input type="radio" name="timep" class="ptime" value="1">
              3M </label>
            <label class="btn btn-default btn-sm  active">
              <input type="radio" name="timep" class="ptime" checked="checked" value="2">
              6M </label>
            <label class="btn btn-default  btn-sm">
              <input type="radio" name="timep" class="ptime" value="4">
              1Y </label>
            <label class="btn btn-default btn-sm">
              <input type="radio" name="timep"  class="ptime"value="5">
              5Y </label>
          </div>
        </div>        
      </div>
      <!-- MAIN CONTENT -->
      
      <div class="row">
        <div class="col-md-12" id="sharkfin_chart">
          <section class="panel">
            <div class="panel-body"> 
              
              <!-- Tab panes -->
              
              <div class="tab-content">
                <div class="tab-pane active" id="tab-chart">
                  <div id="temp"></div>
                  <div id="areachart" style="width:100%; height:900px;"></div>
                </div>
              </div>
              
              <!--/tab-content--> 
              
            </div>
          </section>
          
          <!--/panel--> 
          
        </div>
      </div>
      
      <!-- END MAIN CONTENT--> 
      
      <!-- page end--> 
      
    </section>
  </section>
  
  <!--main content end--> 
  
</section>
<script src="<?php echo $this->config->base_url();?>application/views/js/lib/jquery.js"></script> 
<script src="<?php echo $this->config->base_url();?>application/views/bs3/js/bootstrap.min.js"></script> 
<script class="include" type="text/javascript" src="<?php echo $this->config->base_url();?>application/views/js/accordion-menu/jquery.dcjqaccordion.2.7.js"></script> 
<script src="<?php echo $this->config->base_url();?>application/views/js/scrollTo/jquery.scrollTo.min.js"></script> 
<script src="<?php echo $this->config->base_url();?>application/views/assets/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script> 
<script src="<?php echo $this->config->base_url();?>application/views/js/nicescroll/jquery.nicescroll.js"></script> 

<!--common script init for all pages--> 

<script src="<?php echo $this->config->base_url();?>application/views/assets/select2-master/select2.js"></script> 
<script src="<?php echo $this->config->base_url();?>application/views/js/select2/select-init.js"></script> 

<!--common script init for all pages--> 

<script src="<?php echo $this->config->base_url();?>application/views/assets/easypiechart/jquery.easypiechart.js"></script> 
<script src="<?php echo $this->config->base_url();?>application/views/js/scripts.js"></script> 
<script src="<?php echo $this->config->base_url();?>application/views/chart-js/highstock.js"></script> 
<script src="<?php echo $this->config->base_url();?>application/views/chart-js/exporting.js"></script> 
<script src="<?php echo $this->config->base_url();?>application/views/chart-js/technical-indicators.src.js"></script> 
<script src="<?php echo $this->config->base_url();?>application/views/chart-js/indicators.js"></script> 
<script src="<?php echo $this->config->base_url();?>application/views/chart-js/rsi.js"></script> 
<!--script src="<?php echo $this->config->base_url();?>application/views/chart-js/atr.js"></script-->
<script src="<?php echo $this->config->base_url();?>application/views/chart-js/sma.js"></script> 
<script src="<?php echo $this->config->base_url();?>application/views/chart-js/ema.js"></script> 
<script src="<?php echo $this->config->base_url();?>application/views/chart-js/moment-with-locales.min.js"></script> 
<script src="<?php echo $this->config->base_url();?>application/views/js/angularjs.js"></script> 
<script src="<?php echo $this->config->base_url();?>application/views/js/jquery.blockUI.js"></script> 
<script type="text/javascript">

  





// $("#recent").select2({

//     minimumInputLength: 0

//  });

var base_url='<?php echo base_url(); ?>';



var chart;

function drawchart(tick, ptime, tname, cname, country, tickp) {


    //sharkPeriods

    Highcharts.theme = {

        colors: ["#B40404", "#7798BF", "#55BF3B", "#DF5353", "#aaeeee", "#ff0066", "#eeaaee",

            "#55BF3B", "#DF5353", "#7798BF", "#aaeeee"

        ],

        chart: {

            backgroundColor: {

                linearGradient: {

                    x1: 0,

                    y1: 0,

                    x2: 0,

                    y2: 1

                },

                stops: [

                    [0, 'rgb(96, 96, 96)'],

                    [1, 'rgb(16, 16, 16)']

                ]

            },

            borderWidth: 0,

            borderRadius: 0,

            plotBackgroundColor: '#1C1C1C',

            plotShadow: false,

            plotBorderWidth: 0

        },

        title: {

            style: {

                color: '#FFF',

                font: '16px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'

            }

        },

        subtitle: {

            style: {

                color: '#DDD',

                font: '12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'

            }

        },

        xAxis: {

            gridLineWidth: 0,

            lineColor: '#999',

            tickColor: '#999',

            labels: {

                style: {

                    color: '#999',

                    fontWeight: 'bold'

                }

            },

            title: {

                style: {

                    color: '#AAA',

                    font: 'bold 12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'

                }

            }

        },

        yAxis: {

            alternateGridColor: null,

            minorTickInterval: null,

            gridLineColor: 'rgba(255, 255, 255, .1)',

            minorGridLineColor: 'rgba(255,255,255,0.07)',

            lineWidth: 0,

            tickWidth: 0,

            labels: {

                style: {

                    color: '#999',

                    fontWeight: 'bold'

                }

            },

            title: {

                style: {

                    color: '#AAA',

                    font: 'bold 12px Lucida Grande, Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif'

                }

            }

        },

        legend: {

            itemStyle: {

                color: '#CCC'

            },

            itemHoverStyle: {

                color: '#FFF'

            },

            itemHiddenStyle: {

                color: '#333'

            }

        },

        labels: {

            style: {

                color: '#CCC'

            }

        },

        tooltip: {

            backgroundColor: {

                linearGradient: {

                    x1: 0,

                    y1: 0,

                    x2: 0,

                    y2: 1

                },

                stops: [

                    [0, 'rgba(96, 96, 96, .8)'],

                    [1, 'rgba(16, 16, 16, .8)']

                ]

            },

            borderWidth: 0,

            style: {

                color: '#FFF'

            }

        },

        plotOptions: {

            series: {

                nullColor: '#444444'

            },

            line: {

                dataLabels: {

                    color: '#CCC'

                },

                marker: {

                    lineColor: '#333'

                }

            },

            spline: {

                marker: {

                    lineColor: '#333'

                }

            },

            scatter: {

                marker: {

                    lineColor: '#333'

                }

            },

            candlestick: {

                oxymoronic: true,

                color: '#ff0000',

                lineColor: '#ff0000',

                upColor: '#008000',

                upLineColor: '#008000'

            }

        },



        toolbar: {

            itemStyle: {

                color: '#CCC'

            }

        },



        navigation: {

            buttonOptions: {

                symbolStroke: '#DDDDDD',

                hoverSymbolStroke: '#FFFFFF',

                theme: {

                    fill: {

                        linearGradient: {

                            x1: 0,

                            y1: 0,

                            x2: 0,

                            y2: 1

                        },

                        stops: [

                            [0.4, '#606060'],

                            [0.6, '#333333']

                        ]

                    },

                    stroke: '#000000'

                }

            }

        },



        // scroll charts

        rangeSelector: {

            buttonTheme: {

                fill: {

                    //linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },

                    stops: [

                        [0.4, '#727171'],

                        [0.6, '#727171']

                    ]

                },

                stroke: '#727171',

                style: {

                    color: '#727171',

                    fontWeight: 'bold'

                },

                states: {

                    hover: {

                        fill: {

                            linearGradient: {

                                x1: 0,

                                y1: 0,

                                x2: 0,

                                y2: 1

                            },

                            stops: [

                                [0.4, '#727171'],

                                [0.6, '#727171']

                            ]

                        },

                        stroke: '#727171',

                        style: {

                            color: '#727171'

                        }

                    },

                    select: {

                        fill: {

                            linearGradient: {

                                x1: 0,

                                y1: 0,

                                x2: 0,

                                y2: 1

                            },

                            stops: [

                                [0.1, '#727171'],

                                [0.3, '#727171']

                            ]

                        },

                        stroke: '#727171',

                        style: {

                            color: '#727171'

                        }

                    }

                }

            },

            inputStyle: {

                backgroundColor: '#333',

                color: 'silver'

            },

            labelStyle: {

                color: 'silver'

            }

        },



        navigator: {

            handles: {

                backgroundColor: '#666',

                borderColor: '#AAA'

            },

            outlineColor: '#CCC',

            maskFill: 'rgba(16, 16, 16, 0.5)',

            series: {

                color: '#7798BF',

                lineColor: '#A6C7ED'

            }

        },



        scrollbar: {

            barBackgroundColor: {

                linearGradient: {

                    x1: 0,

                    y1: 0,

                    x2: 0,

                    y2: 1

                },

                stops: [

                    [0.4, '#888'],

                    [0.6, '#555']

                ]

            },

            barBorderColor: '#CCC',

            buttonArrowColor: '#CCC',

            buttonBackgroundColor: {

                linearGradient: {

                    x1: 0,

                    y1: 0,

                    x2: 0,

                    y2: 1

                },

                stops: [

                    [0.4, '#888'],

                    [0.6, '#555']

                ]

            },

            buttonBorderColor: '#CCC',

            rifleColor: '#FFF',

            trackBackgroundColor: {

                linearGradient: {

                    x1: 0,

                    y1: 0,

                    x2: 0,

                    y2: 1

                },

                stops: [

                    [0, '#000'],

                    [1, '#333']

                ]

            },

            trackBorderColor: '#666'

        },



        // special colors for some of the demo examples

        legendBackgroundColor: 'rgba(48, 48, 48, 0.8)',

        background2: 'rgb(70, 70, 70)',

        dataLabelsColor: '#444',

        textColor: '#E0E0E0',

        maskColor: 'rgba(255,255,255,0.3)'

    };



    // Apply the theme

    Highcharts.setOptions(Highcharts.theme);



    Highcharts.setOptions({

        lang: {

            rangeSelectorZoom: ""

        }

    });



    $.getJSON('<?php echo $this->config->site_url()?>/getdata/get/' + tick, function(data) {

        var ohlc = [],

            cprice = [],

            volume = [],

            dataLength = data.length;



        // create points for 6 months in array

        var maxclosepre6m = Array.apply(null, new Array(10)).map(Number.prototype.valueOf, 0);

        var maxclosedatepre6m = Array.apply(null, new Array(10)).map(Number.prototype.valueOf, 0);

        var aa;

        var maxclosepre6mpos = [];



        for (var i6 = 10; i6 >= 0; i6--) {

            aa = moment().subtract("months", i6 * 6).format("YYYY-MM-DD").split('-');

            maxclosepre6mpos.push(Date.UTC(aa[0], parseInt(aa[1]) - 1, aa[2]));



        }

        var minclosepre6m = Array.apply(null, new Array(10)).map(Number.prototype.valueOf, 0);

        var minclosedatepre6m = Array.apply(null, new Array(10)).map(Number.prototype.valueOf, 0);

        // end of points 6 m  

        // create points for 3 months in array

        var maxclosepre3m = Array.apply(null, new Array(20)).map(Number.prototype.valueOf, 0);

        var maxclosedatepre3m = Array.apply(null, new Array(20)).map(Number.prototype.valueOf, 0);

        var maxclosepre3mpos = [];



        for (i6 = 20; i6 >= 0; i6--) {

            aa = moment().subtract("months", i6 * 3).format("YYYY-MM-DD").split('-');

            maxclosepre3mpos.push(Date.UTC(aa[0], parseInt(aa[1]) - 1, aa[2]));



        }

        var minclosepre3m = Array.apply(null, new Array(20)).map(Number.prototype.valueOf, 0);

        var minclosedatepre3m = Array.apply(null, new Array(20)).map(Number.prototype.valueOf, 0);

        // end of points 3 m   

        // create points for 1 month in array

        var maxclosepre1m = Array.apply(null, new Array(60)).map(Number.prototype.valueOf, 0);

        var maxclosedatepre1m = Array.apply(null, new Array(60)).map(Number.prototype.valueOf, 0);

        var maxclosepre1mpos = [];

        for (i6 = 60; i6 >= 0; i6--) {

            aa = moment().subtract("months", i6 * 1).format("YYYY-MM-DD").split('-');

            maxclosepre1mpos.push(Date.UTC(aa[0], parseInt(aa[1]) - 1, aa[2]));

        }

        var minclosepre1m = Array.apply(null, new Array(60)).map(Number.prototype.valueOf, 0);

        var minclosedatepre1m = Array.apply(null, new Array(60)).map(Number.prototype.valueOf, 0);

        // end of points 1 m



        for (i = 0; i < dataLength; i++) {



            // High price and Low price for previous 6 month

            for (var j = 1; j < maxclosepre6mpos.length; j++) {

                if (data[i][0] > maxclosepre6mpos[j - 1] && data[i][0] <= maxclosepre6mpos[j]) {



                    // make point for high value

                    if (maxclosepre6m[j - 1] < data[i][4]) {

                        if (data[i][4] < data[i][1]) {

                            maxclosepre6m[j - 1] = data[i][1];

                        } else {

                            maxclosepre6m[j - 1] = data[i][4];

                        }



                        maxclosedatepre6m[j - 1] = data[i][0];

                    }

                    // make point for low value

                    if (minclosepre6m[j - 1] > data[i][4] || minclosepre6m[j - 1] == 0) {

                        if (data[i][4] > data[i][1]) {

                            minclosepre6m[j - 1] = data[i][1];

                        } else {

                            minclosepre6m[j - 1] = data[i][4];

                        }



                        minclosedatepre6m[j - 1] = data[i][0];

                    }



                }

            }

            // End of previous 6 months



            // High price and Low price for previous 3 months

            for (j = 1; j < maxclosepre3mpos.length; j++) {

                if (data[i][0] > maxclosepre3mpos[j - 1] && data[i][0] <= maxclosepre3mpos[j]) {



                    // make point for high value

                    if (maxclosepre3m[j - 1] < data[i][4]) {

                        if (data[i][4] < data[i][1]) {

                            maxclosepre3m[j - 1] = data[i][1];

                        } else {

                            maxclosepre3m[j - 1] = data[i][4];

                        }

                        maxclosedatepre3m[j - 1] = data[i][0];

                    }

                    // make point for low value

                    if (minclosepre3m[j - 1] > data[i][4] || minclosepre3m[j - 1] == 0) {

                        if (data[i][4] > data[i][1]) {

                            minclosepre3m[j - 1] = data[i][1];

                        } else {

                            minclosepre3m[j - 1] = data[i][4];

                        }



                        minclosedatepre3m[j - 1] = data[i][0];

                    }



                }

            }

            // End of previous 3 months



            // High price and Low price for previous 1 month

            for (j = 1; j < maxclosepre1mpos.length; j++) {

                if (data[i][0] > maxclosepre1mpos[j - 1] && data[i][0] <= maxclosepre1mpos[j]) {



                    // make point for high value

                    if (maxclosepre1m[j - 1] < data[i][4]) {

                        if (data[i][4] < data[i][1]) {

                            maxclosepre1m[j - 1] = data[i][1];

                        } else {

                            maxclosepre1m[j - 1] = data[i][4];

                        }

                        maxclosedatepre1m[j - 1] = data[i][0];

                    }

                    // make point for low value

                    if (minclosepre1m[j - 1] > data[i][4] || minclosepre1m[j - 1] == 0) {

                        if (data[i][4] > data[i][1]) {

                            minclosepre1m[j - 1] = data[i][1];

                        } else {

                            minclosepre1m[j - 1] = data[i][4];

                        }



                        minclosedatepre1m[j - 1] = data[i][0];

                    }



                }

            }

            // End of previous 1 months



            ohlc.push([

                data[i][0], // the date

                data[i][1], // open

                data[i][2], // high

                data[i][3], // low

                data[i][4], // close

                data[i][5] // volume

            ]);



            cprice.push([

                data[i][0], // the date

                data[i][4] // the volume

            ]);



            volume.push([

                data[i][0], // the date

                data[i][5] // the volume

            ]);



        }



        // condition for max 6m

        var maxmark6m = [];

        for (var k = 0; k < maxclosepre6m.length - 1; k++) {

            if (maxclosepre6m[k] > maxclosepre6m[k + 1]) {

                maxmark6m.push({



                    x: maxclosedatepre6m[k + 1],

                    y: maxclosepre6m[k + 1],

                    marker: {

                        symbol: 'url(http://108.170.54.199/~nodoma17/demo/sharkfin/images/up.png)'

                    }

                });

            }

        }

        //------------------------------

        // condition for min 6m

        var minmark6m = [];



        for (var k = 0; k < minclosepre6m.length - 1; k++) {

            if (minclosepre6m[k] < minclosepre6m[k + 1]) {

                minmark6m.push({



                    x: minclosedatepre6m[k + 1],

                    y: minclosepre6m[k + 1],

                    marker: {

                        symbol: 'url(http://108.170.54.199/~nodoma17/demo/sharkfin/images/down.png)'

                    }

                });

            }

        }

        //---------------------------------------



        // condition for max 3m

        var maxmark3m = [];

        for (var k = 0; k < maxclosepre3m.length - 1; k++) {

            if (maxclosepre3m[k] > maxclosepre3m[k + 1]) {

                maxmark3m.push({



                    x: maxclosedatepre3m[k + 1],

                    y: maxclosepre3m[k + 1],

                    marker: {

                        symbol: 'url(http://108.170.54.199/~nodoma17/demo/sharkfin/images/up.png)'

                    }

                });

            }

        }

        //------------------------------

        // condition for min 3m

        var minmark3m = [];



        for (var k = 0; k < minclosepre3m.length - 1; k++) {

            if (minclosepre3m[k] < minclosepre3m[k + 1]) {

                minmark3m.push({



                    x: minclosedatepre3m[k + 1],

                    y: minclosepre3m[k + 1],

                    marker: {

                        symbol: 'url(http://108.170.54.199/~nodoma17/demo/sharkfin/images/down.png)'

                    }

                });

            }

        }

        //---------------------------------------



        // condition for max 1m

        var maxmark1m = [];

        for (var k = 0; k < maxclosepre1m.length - 1; k++) {

            if (maxclosepre1m[k] > maxclosepre1m[k + 1]) {

                maxmark1m.push({



                    x: maxclosedatepre1m[k + 1],

                    y: maxclosepre1m[k + 1],

                    marker: {

                        symbol: 'url(http://108.170.54.199/~nodoma17/demo/sharkfin/images/up.png)'

                    }

                });

            }

        }

        //------------------------------

        // condition for min 1m

        var minmark1m = [];



        for (var k = 0; k < minclosepre1m.length - 1; k++) {

            if (minclosepre1m[k] < minclosepre1m[k + 1]) {

                minmark1m.push({



                    x: minclosedatepre1m[k + 1],

                    y: minclosepre1m[k + 1],

                    marker: {

                        symbol: 'url(http://108.170.54.199/~nodoma17/demo/sharkfin/images/down.png)'

                    }

                });

            }

        }

        //---------------------------------------



        //---------------------------------------

        //alert(maxclosepre1m + ' : ' + maxclosecurr1m);

        //alert(minclosepre1m + ' : ' + minclosecurr1m);



        var groupingUnits = [

            [

                'week', // unit name

                [1] // allowed multiples

            ],

            [

                'month', // unit name

                [1, 3, 6] // allowed multiples

            ]

        ];



        function hideZoomBar(chart) {

            chart.rangeSelector.zoomText.hide();

            $.each(chart.rangeSelector.buttons, function() {

                this.hide();

            });

            $(chart.rangeSelector.divRelative).hide();

            chart.scroller.xAxis.labelGroup.hide();

            chart.scroller.xAxis.gridGroup.hide();

            chart.scroller.series.hide();

            chart.scroller.scrollbar.hide();

            chart.scroller.scrollbarGroup.hide();

            chart.scroller.navigatorGroup.hide();

            $.each(chart.scroller.elementsToDestroy, function(i, elem) {

                elem.hide();

            });



        }



        chart = new Highcharts.StockChart({

            chart: {

                renderTo: 'areachart'

            },

            // loading: {

            //     labelStyle: {

            //         backgroundImage: 'url("http://108.170.54.199/~nodoma17/demo/sharkfin/images/wait.gif")',

            //         display: 'block',

            //         width: '200px',

            //         height: '50px',

            //         backgroundColor: '#000'

            //     }

            // },



            navigator: {

                adaptToUpdatedData: true,

                series: {

                    data: data

                }

            },

            credits: {

                enabled: false

            },

            rangeSelector: {

                //enabled: false,

                selected: ptime

            },

            title: {

                text: tname

            },

            subtitle: {

                text: cname + country,

                align: 'center',

                x: -10

            },

            yAxis: [{

                opposite: false,

                title: {

                    text: tick

                },

                top: 120,

                height: 400,

                lineWidth: 2

            }, {

                opposite: false,

                title: {

                    text: 'MACD'

                },

                top: 525,

                height: 100,

                offset: 0,

                lineWidth: 2

            }, {

                opposite: false,

                title: {

                    text: 'Volume'

                },

                top: 730,

                height: 100,

                offset: 0,

                lineWidth: 2

            }],

            indicators: [{

                id: 'mdata',

                type: 'rsi',

                params: {

                    period: 14,

                    overbought: 70,

                    oversold: 30

                },

                styles: {

                    strokeWidth: 1,

                    stroke: 'white'

                },

                yAxis: {

                    lineWidth: 2,

                    opposite: false,

                    title: {

                        text: 'RSI',

                        x: -27

                    },

                    top: 730,

                    height: 100

                }

            }],

            tooltip: {

                //crosshairs: [true, true],

                enabledIndicators: true

            },





            series: [{

                    cropThreshold: 0,

                    type: 'candlestick',

                    id: 'mdata',

                    name: tick,

                    data: ohlc,

                    dataGrouping: {

                        enabled: false,

                        units: groupingUnits

                    }

                }, {

                    name: '20-day SMA',

                    linkedTo: 'primary',

                    showInLegend: true,

                    color: '#F3F781',

                    lineWidth: 1,

                    type: 'trendline',

                    algorithm: 'SMA',

                    periods: 20

                }, {

                    name: '50-day SMA',

                    linkedTo: 'primary',

                    showInLegend: true,

                    color: '#64FE2E',

                    lineWidth: 1,

                    type: 'trendline',

                    algorithm: 'SMA',

                    periods: 50

                }, {

                    name: '200-day SMA',

                    linkedTo: 'primary',

                    color: '#FF0080',

                    lineWidth: 1,

                    showInLegend: true,

                    type: 'trendline',

                    algorithm: 'SMA',

                    periods: 200

                },





                {



                    name: 'Price',

                    id: 'primary',

                    visible: false,

                    data: cprice,

                    dataGrouping: {

                        enabled: false,

                        units: groupingUnits

                    }

                }, {

                    type: 'column',

                    name: 'Volume',

                    yAxis: 2,

                    data: volume,

                    dataGrouping: {

                        enabled: false,

                        units: groupingUnits

                    }

                }, {

                    type: "scatter",

                    enableMouseTracking: false,

                    data: maxmark6m,



                }, {

                    type: "scatter",

                    enableMouseTracking: false,

                    data: minmark6m,



                }, {

                    type: "scatter",

                    enableMouseTracking: false,

                    data: maxmark3m,



                }, {

                    type: "scatter",

                    enableMouseTracking: false,

                    data: minmark3m,



                }, {

                    type: "scatter",

                    enableMouseTracking: false,

                    data: maxmark1m,



                }, {

                    type: "scatter",

                    enableMouseTracking: false,

                    data: minmark1m,



                },



                {

                    name: 'MACD',

                    linkedTo: 'primary',

                    yAxis: 1,

                    showInLegend: true,

                    type: 'trendline',

                    algorithm: 'MACD'



                }, {

                    name: 'Signal line',

                    linkedTo: 'primary',

                    yAxis: 1,

                    showInLegend: true,

                    type: 'trendline',

                    algorithm: 'signalLine'



                }, {

                    name: 'Histogram',

                    linkedTo: 'primary',

                    yAxis: 1,

                    showInLegend: true,

                    type: 'histogram'



                }

            ]

        });



        hideZoomBar(chart);



        switch (tickp) {

            case '1':

                chart.series[6].hide();

                chart.series[7].hide();

                chart.series[8].hide();

                chart.series[9].hide();

                chart.series[10].show();

                chart.series[11].show();

                break;

            case '3':

                chart.series[6].hide();

                chart.series[7].hide();

                chart.series[8].show();

                chart.series[9].show();

                chart.series[10].hide();

                chart.series[11].hide();

                break;

            case '6':

                chart.series[6].show();

                chart.series[7].show();

                chart.series[8].hide();

                chart.series[9].hide();

                chart.series[10].hide();

                chart.series[11].hide();

                break;

            default:

                chart.series[6].hide();

                chart.series[7].hide();

                chart.series[8].hide();

                chart.series[9].hide();

                chart.series[10].hide();

                chart.series[11].hide();

                break;

        }

        // chart.hideLoading();
          $('#sharkfin_chart').unblock();
    });

  

}



 function movieFormatSelection(movie) {
        return movie.ticker_name;
    }

$(document).ready(function() {

    //$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    $("#symSearch").select2({
        minimumInputLength: 0,
        ajax: {
            url: base_url+'index.php/dashboard/getSymbols',
            dataType: 'json',
            type: "GET",
            data: function (term) {
                return {
                    term: term
                };
            },

            results: function (data) {

                return {

                    results: $.map(data, function (item) {

                        return {

                            text: item.ticker_name,

                            slug: item.symbol_id,

                            id: item.symbol_id,

                            company: item.company_name

                        }

                    })

                };

            },



        },



        formatResult: function (data) {

            // console.log(data);

                return "<b>" + data.text + "</b> "+data.company ;

        }

    });



    $("#historySearch").select2({

        minimumInputLength: 0,

        ajax: {

            url: base_url+'index.php/dashboard/recent_search',

            dataType: 'json',

            type: "GET",

            data: function (term) {

                return {

                    term: term

                };

            },

            results: function (data) {

                return {

                    results: $.map(data, function (item) {

                        return {

                            text: item.ticker_name,

                            slug: item.symbol_id,

                            id: item.symbol_id,

                            company: item.company_name

                        }

                    })

                };

            },



        },



        formatResult: function (data) {

            // console.log(data);

                return "<b>" + data.text + "</b> "+data.company ;

        }

    });



	$('.fa-bars').click(function(){

		//alert("Hello");

		setTimeout(function(){$(window).trigger('resize');},300);

		    

		});

//    $("#e3").select2({

//        minimumInputLength: 0

//     });

		

	$("#e3").select2({

                        minimumInputLength: 0

                     });

    

    

	

	//$("#dsub-menu").height($(window).height()-145).css({"overflow-x":"hidden", "overflow-y":"auto"});

 

      var ti = 'APB';

      var tname = 'APB Name';

      var tcat = 'category';

      var tcountry = 'country';



 	 // drawchart(ti, 2, tname, tcat, tcountry, 1);  // for default display 





	    $('.ftime').change(function(){

	    	// chart.showLoading();

		var tickp = $('input[name=tickp]:checked').val();

    	var tick = $('#e3').val();

    	switch(tickp){

    	case '3':

        	chart.series[6].hide();

        	chart.series[7].hide();

        	chart.series[8].show();

        	chart.series[9].show();

        	chart.series[10].hide();

        	chart.series[11].hide();

        	break;

    	case '6':

    		chart.series[6].show();

        	chart.series[7].show();

        	chart.series[8].hide();

        	chart.series[9].hide();

        	chart.series[10].hide();

        	chart.series[11].hide();

        	break;

        default:

            chart.series[6].hide();

        	chart.series[7].hide();

        	chart.series[8].hide();

        	chart.series[9].hide();

        	chart.series[10].show();

        	chart.series[11].show();

            break;

    	}

    	// chart.hideLoading();
        $('#sharkfin_chart').unblock();

       	}); 



    $('.ptime').change(function(){

    	var timep = $('input[name=timep]:checked').val();

         switch(timep){

         case '0':

        	 chart.series[0].update({ dataGrouping: { enabled: false } });

             var timetype = "months";

             var typeval = 1

             break;

         case '1':

        	 chart.series[0].update({ dataGrouping: { enabled: false } });

             var timetype = "months";

             var typeval = 3

             break;

         case '2':

        	 chart.series[0].update({ dataGrouping: { enabled: false } });

        	 var timetype = "months";

             var typeval = 6

             break;

         case '4':

        	 chart.series[0].update({ dataGrouping: { enabled: false } });

        	 var timetype = "years";

             var typeval = 1

             break;

         case '5':

        	 chart.series[0].update({ dataGrouping: { enabled: true } });

        	 var timetype = "years";

             var typeval = 5

             break;

         default :

        	 chart.series[0].update({ dataGrouping: { enabled: false } });

        	 var timetype = "months";

             var typeval = 1

             break;

         }

    	var aa=moment().subtract(timetype,typeval).format("YYYY-MM-DD").split('-');

        var datefrom  = Date.UTC(aa[0],parseInt(aa[1])-1,aa[2]);

    	   	

    	// chart.showLoading();

    	chart.xAxis[0].setExtremes(datefrom);

    	//alert(chart.xAxis.length);

    	// chart.hideLoading();
        $('#sharkfin_chart').unblock();

    	/*var timep = $('input[name=timep]:checked').val();

    	var tickp = $('input[name=tickp]:checked').val();

    	var tick = $('#e3').val();

       	$.getJSON('http://localhost/sharkfin/get-symbolde.php?sym='+ tick , function(data) {

            mdata = data;

            drawchart(tick, timep, mdata[0],mdata[1],mdata[2], tickp);

		 }); */

        }); 



    $("#symSearch").change(function(){ 

        // console.log(this);
        $("[name=tickp]").attr("checked", false);

        $("#sharkFins label").removeClass("active");
        $("#sharkPeriods label").removeClass("active");
        
        var periodClass= $("#sharkPeriods label")[2];
        $(periodClass).addClass("active");
        
        $('#sharkfin_chart').unblock();
        $('#sharkfin_chart').block(
        {
            message: '<h1>Processing...</h1>',
            css: { border: '0px solid #a00' }
        });

        var tickp = $('input[name=tickp]:checked').val();

        var timep = $('input[name=timep]:checked').val();

        var tick = $(this).val();



        console.log(this);



        $.getJSON('<?php echo $this->config->site_url()?>/getdata/getsymbol/'+ tick , function(data) 

        {



            mdata = data;

            // angular.element($(this)).scope().getScoreData(tick);

            drawchart(mdata[3], 2, mdata[0],mdata[1],mdata[2], tickp);
             
         });

       

    }); 





    $("#historySearch").change(function(){ 

        // console.log(this);
        
         $("[name=tickp]").attr("checked", false);

        $("#sharkFins label").removeClass("active");
        $("#sharkPeriods label").removeClass("active");
        
        var periodClass= $("#sharkPeriods label")[2];
        $(periodClass).addClass("active");
        
        $('#sharkfin_chart').unblock();
        $('#sharkfin_chart').block(
        {
            message: '<h1>Processing...</h1>',
            css: { border: '0px solid #a00' }
        });



        var tickp = $('input[name=tickp]:checked').val();

        var timep = $('input[name=timep]:checked').val();

        var tick = $(this).val();

       

        $.getJSON('<?php echo $this->config->site_url()?>/getdata/getsymbol/'+ tick , function(data) 

        {



            mdata = data;

            // angular.element($(this)).scope().getScoreData(tick);

            drawchart(mdata[3], 2, mdata[0],mdata[1],mdata[2], tickp);
             
         });

       

    });  

	 


	

});


$(window).resize(function() {
        
        setTimeout(function(){$(window).trigger('resize');},300);
    });

</script> 
<script type="text/javascript">

//    var sharkFinApp=angualr.module("sharkFinApp",[]);

//    

//    sharkFinApp.controller("sharkFinController",function($scope){

//        alert("Hello World");

//    });

    

    function sharkController($scope,$http){

        console.log("Hello");

        $scope.recentSearch=[];



                // $http({

                //     url: '<?php echo base_url(); ?>index.php/dashboard/stock_symbols',

                //     method: "GET"

                // })

                // .then(function(response) {

                    

                //     $scope.symbols=response.data;

                //     console.log($scope.symbols);



                // }, 

                // function(response) { // optional

                //         // failed

                //         console.log('failed');

                //     }

                // );



                $http({

                    url: '<?php echo base_url(); ?>index.php/dashboard/recent_search',

                    method: "GET"

                })

                .then(function(response) {

                    

                    $scope.recentSearch=response.data;

                    console.log($scope.symbols);

                   

                }, 

                function(response) { // optional

                        // failed

                        console.log('failed');

                    }

                );





        // $scope.getScoreData=function(obj){

        //     console.log(obj);

        //      $http({

        //             url: '<?php echo $this->config->site_url()?>/getdata/getsymbol/'+ obj.ticker_name,

        //             method: "GET"

        //         })

        //         .then(function(response) {

        //             var tickp = $('input[name=tickp]:checked').val();

        //             var timep = $('input[name=timep]:checked').val();

                    

        //             var mdata = response.data;

        //             chart.showLoading();

        //             drawchart(obj.ticker_name, timep, mdata[0],mdata[1],mdata[2], tickp);

        //         }, 

        //         function(response) { // optional

        //                 // failed

        //                 console.log('failed');

        //             }

        //         );

        // }

        $scope.getScoreData=function(obj){

            console.log("$scope.getScoreData --- " );

            $http({

                    url: '<?php echo base_url(); ?>index.php/dashboard/recent_search',

                    method: "GET"

                })

                .then(function(response) {

                    

                    $scope.recentSearch=response.data;

                    console.log($scope.symbols);

                   

                }, 

                function(response) { // optional

                        // failed

                        console.log('failed');

                    }

                );



        }



        $scope.recentSelected=function(){

            if($scope.recentModel){

                console.log("$scope.recentModel" + $scope.recentModel)

                // chart.showLoading();

                var tickp = $('input[name=tickp]:checked').val();

                var timep = $('input[name=timep]:checked').val();

                var tick = $scope.recentModel;

               

                

                $.getJSON('<?php echo $this->config->site_url()?>/getdata/getsymbol/'+ tick , function(data) 

                {

                    mdata = data;

                    drawchart(tick, timep, mdata[0],mdata[1],mdata[2], tickp);

                 });

               

            }

        }

        

        

    }

    

</script> 
</body>
</html>