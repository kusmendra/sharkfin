<body class="login-body">

    <div class="container">
          <?php
		$attributes = array('class' => 'form-signin', 'id' => 'myform');
		echo form_open('/verifylogin', $attributes);
	?>

            <h2 class="form-signin-heading">sign in now</h2>
            <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
            <div class="login-wrap">
                <div class="user-login-info">
                    <input type="text" class="form-control" name="username" placeholder="Email ID" autofocus>
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
                <label class="checkbox">
                    <input type="checkbox" value="remember-me">Remember me
                    <span class="pull-right">
                        <a data-toggle="modal" href="#myModal"> Forgot Password?</a>

                    </span>
                </label>
                <button class="btn btn-lg btn-login btn-block" type="submit">Sign in</button>

                <div class="registration">
                    Don't have an account yet?
                    <a class="" href="<?php echo $this->config->site_url();?>/login/registration">
                    Create an account
                </a>
                </div>

            </div>

            <!-- Modal -->
            <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Forgot Password ?</h4>
                        </div>
                        <div class="modal-body">
                            <p>Enter your e-mail address below to reset your password.</p>
                            <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">

                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                            <button class="btn btn-info" type="button">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- modal -->

        </form>

    </div>





</body>



</html>