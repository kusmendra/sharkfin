// Use Morris.Area instead of Morris.Line
Morris.Area({
    element: 'graph-area-line',
    behaveLikeLine: false,
    data: [
        {x: '2011 Q1', y: 100, z: 300},
        {x: '2011 Q2', y: 200, z: 100},
        {x: '2011 Q3', y: 200, z: 400},
        {x: '2011 Q4', y: 300, z: 300},
        {x: '2011 Q5', y: 300, z: 400}
    ],
    xkey: 'x',
    ykeys: ['y', 'z'],
    labels: ['Sales', 'IT'],
    lineColors:['#E67A77','#79D1CF']



});


