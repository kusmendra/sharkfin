Morris.Bar({
    element: 'graph-bar',
    data: [
        {x: '', y: 3, z: 2, a: 1}
    ],
    xkey: 'x',
    ykeys: ['y', 'z', 'a'],
    labels: ['Received', 'Sent', 'Deleted'],
    barColors:['#53D858','#FACC2E','#F55C3B']


});



var day_data = [
    {"elapsed": "Private", "value": 34},
    {"elapsed": "Dept1 UK", "value": 3},
    {"elapsed": "Tech", "value": 12},
    {"elapsed": "Design", "value": 13},
    {"elapsed": "Sales", "value": 22}
];
var day_data1 = [
    {"elapsed": "New", "value": 34},
    {"elapsed": "Active", "value": 3},
    {"elapsed": "Pending", "value": 12},
    {"elapsed": "Inactive", "value": 13},
    {"elapsed": "Deleted", "value": 22}
];
Morris.Line({
    element: 'graph-line1',
    data: day_data,
    xkey: 'elapsed',
    ykeys: ['value'],
    labels: ['value'],
    lineColors:['#38bbeb'],
    parseTime: false
});

Morris.Line({
    element: 'graph-line2',
    data: day_data1,
    xkey: 'elapsed',
    ykeys: ['value'],
    labels: ['value'],
    lineColors:['#38bbeb'],
    parseTime: false
});


// Use Morris.Area instead of Morris.Line DONUT1
Morris.Donut({
    element: 'graph-donut1',
    data: [
        {value: 85, label: '85%', formatted: 'Adoption' },
       {value: 15, label: '73%', formatted: 'Adoption' }
    ],
    backgroundColor: '#fff',
    labelColor: '#767676',
    colors: [
        '#53D858','#CCCCCC','#79D1CF','#95D7BB'
    ],
    formatter: function (x, data) { return data.formatted; }
});

Morris.Donut({
    element: 'graph-donut2',
    data: [
        {value: 67, label: '67%', formatted: 'Adoption' },
       {value: 33, label: '34%', formatted: 'Adoption' }
    ],
    backgroundColor: '#fff',
    labelColor: '#1fb5ac',
    colors: [
        '#FACC2E','#CCCCCC','#79D1CF','#95D7BB'
    ],
    formatter: function (x, data) { return data.formatted; }
});

Morris.Donut({
    element: 'graph-donut3',
    data: [
        {value: 27, label: '27%', formatted: 'Adoption' },
       {value: 73, label: '27%', formatted: 'Adoption' }
    ],
    backgroundColor: '#fff',
    labelColor: '#1fb5ac',
    colors: [
        '#F55C3B','#CCCCCC','#79D1CF','#95D7BB'
    ],
    formatter: function (x, data) { return data.formatted; }
});







