

<body class="login-body">

    <div class="container">

        <form class="form-signin" action="" class="form-signin" method="post">
            <h2 class="form-signin-heading">registration now</h2>
            <div class="login-wrap">
                <p>Enter your personal details below</p>
                 <?php
        if(isset($msg)){
         echo '<div class="row"> <div class="alert alert-success fade in alert-dismissable">'.$msg.'</div></div>';
        }


       ?>
                <?php echo validation_errors('<div class="alert alert-danger">', '</div>'); ?>
                <input type="text" class="form-control" name="fullname" placeholder="Full Name" autofocus>
                <input type="text" class="form-control" name="address" placeholder="Address" autofocus>
                <input type="text" class="form-control" name="email" placeholder="Email" autofocus>
                <input type="text" class="form-control" name="city" placeholder="City/Town" autofocus>
                <div class="radios" >
                    <label class="label_radio col-lg-6 col-sm-6" for="radio-01">
                        <input name="gender" id="radio-01" value="male" type="radio" checked />Male
                    </label>
                    <label class="label_radio col-lg-6 col-sm-6" for="radio-02">
                        <input name="gender" id="radio-02" value="female" type="radio" />Female
                    </label>
                </div>

                <p>Enter your account details below</p>
                <input type="text" name="username" class="form-control" placeholder="User Name" autofocus>
                <input type="password" name="password" class="form-control" placeholder="Password">
                <input type="password" name="repassword" class="form-control" placeholder="Re-type Password">
                <label class="checkbox">
                    <input type="checkbox" name="agree" value="agree this condition">I agree to the Terms of Service and Privacy Policy
                </label>
                <button class="btn btn-lg btn-login btn-block" name="submit" value="submit" type="submit">Submit</button>

                <div class="registration">
                    Already Registered.
                    <a class="" href="<?php echo $this->config->site_url();?>/login">
                    Login
                </a>
                </div>

            </div>

        </form>

    </div>


    <!-- Placed js at the end of the document so the pages load faster -->

    <!--Core js-->


</body>

</html>
