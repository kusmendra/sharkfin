<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="shortcut icon" href="images/favicon.png">
<title>SHARKFIN TRADERS</title>
    <link href="<?php echo $this->config->base_url();?>application/views/bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $this->config->base_url();?>application/views/css/bootstrap-reset.css" rel="stylesheet">
    <link href="<?php echo $this->config->base_url();?>application/views/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo $this->config->base_url();?>application/views/assets/morris-chart/morris.css">
    <!-- Custom styles for this template -->
    <link href="<?php echo $this->config->base_url();?>application/views/css/style.css" rel="stylesheet">
    <link href="<?php echo $this->config->base_url();?>application/views/css/style-responsive.css" rel="stylesheet" />
    <link href="<?php echo $this->config->base_url();?>application/views/css/blue-theme.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->config->base_url();?>application/views/assets/select2-master/select2.css" />
    
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="<?php echo $this->config->base_url();?>application/views/js/ie8-responsive-file-warning.js"></script><![endif]-->
    
    <!--common script init for all pages-->
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>